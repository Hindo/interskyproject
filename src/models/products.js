var query = require('./query'),
	products = require('./data/products');
	
module.exports = query(products);