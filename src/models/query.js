module.exports = query; 

function query(dataObject) {
	var data = {};
	isObject(dataObject);
	data = dataObject;
	
	function filter(queryObject) {
		isObject(queryObject);
		return data.filter(function(item) {
			return item[queryObject.property] === queryObject.value || item[queryObject.property] === undefined;
		})
	}
	
	function filterBy(p, v) {
		return filter({'property': p, 'value':v});
	}
	
	function get(queryObject) {
		isObject(queryObject);
		return data.filter(function(item) {
			return item[queryObject.property] === queryObject.value;
		})[0];
	}
	
	function getBy(p, v) {
		return get({'property': p, 'value':v});
	}
	
	function all() {
		return data;
	}
	
	return {
		filter: filter,
		filterBy: filterBy,
		get: get,
		getBy: getBy,
		all: all
	}
}

function isObject(obj) {
	if (typeof obj !== 'object' || obj === {}) {
		throw Error('Wrong or empty data object provided!');
	}
}