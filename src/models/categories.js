var query = require('./query'),
	categories = require('./data/categories');
	
module.exports = query(categories);