var Query = require('./query'),
	locations = require('./data/locations');

module.exports = Query(locations);
