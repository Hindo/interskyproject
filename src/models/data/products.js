module.exports = [{
	id: 1,
	categoryId: 1,
	locationId: 1,
	label: 'Arsenal TV'
}, {
	id: 2,
	categoryId: 1,
	locationId: 1,
	label: 'Chelsea TV'
}, {
	id: 3,
	categoryId: 1,
	locationId: 2,
	label: 'Liverpool TV'
}, {
	id: 4,
	categoryId: 2,
	locationId: undefined,
	label: 'Sky News'
}, {
	id: 5,
	categoryId: 2,
	locationId: undefined,
	label: 'Sky Sports News'
}];