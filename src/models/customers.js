var query = require('./query'),
	customers = require('./data/customers');

module.exports = query(customers); 
