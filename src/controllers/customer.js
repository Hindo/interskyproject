var customers = require('../models/customers');

module.exports.controller = function(app) {
	app.get('/customer/', function(req, res) {
		var customer = customers.all();
		res.send(JSON.stringify(customer));
	});
	
	app.get('/customer/:id', function(req, res) {
		var id = parseInt(req.params.id, 10);		
		var customer = customers.getBy('id', id);
		res.send(JSON.stringify(customer));
	});
}