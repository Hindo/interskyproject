var products = require('../models/products');

module.exports.controller = function(app) {
	app.get('/catalogue/:id', function(req, res) {
		var locationId = parseInt(req.params.id, 10);	
		var customer = products.filterBy('locationId', locationId);
		res.send(JSON.stringify(customer));
	});
}