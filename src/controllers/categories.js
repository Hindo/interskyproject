var categories = require('../models/categories');

module.exports.controller = function(app) {
	app.get('/categories/', function(req, res) {
		var category = categories.all();
		res.send(JSON.stringify(category));
	});
}