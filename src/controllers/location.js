var locations = require('../models/locations');

module.exports.controller = function(app) {
	app.get('/location/:id', function(req, res) {
		var id = parseInt(req.params.id, 10);
		var location = locations.getBy('id', id);
		res.send(JSON.stringify(location));
	});
}