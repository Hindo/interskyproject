var gulp = require('gulp');
var gutil = require('gulp-util');
var source = require('vinyl-source-stream');
var browserify = require('browserify');
var watchify = require('watchify');

gulp.task('default', function() {
	var bundler = watchify(browserify({
		entries: ['./assets/js/app.js'],
		extensions: ['.js'],
		debug: true,
		cache: {},
		packageCache: {},
		fullPaths: true
	}));
	
	function build(file) {
		if (file) gutil.log('Recompiling ' + file);
		return bundler
			.bundle()
			.on('error', gutil.log.bind(gutil, 'Browserify error'))
			.pipe(source('app.js'))
			.pipe(gulp.dest('./public/js/'))
	}
	
	build();
	bundler.on('update', build);
});