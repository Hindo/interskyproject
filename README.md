# Sample app #

Project for interview purpose

* Version 1.0.0

## Set up ##

### Configuration ###

Please make sure you are using node in version >= 0.12

To get dependencies run command

```
npm install
```

### Run instructions ###

Run command

```
node app.js
```

### How to run tests ###

No test are provided