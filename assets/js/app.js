var angular = require('angular');

require('angular-route');
require('./services');
require('./directives');
require('./controllers');

angular.module('myApp', [
		'myApp.services',
		'myApp.directives',
		'myApp.controllers',
		'ngRoute'
	])
	.config(['$routeProvider' , function($routeProvider) {
		$routeProvider
			.when('/', {
				templateUrl: 'views/login.html',
				controller: 'LoginController',
				resolve: {
					customers: ['Customer', function(Customer) {
						return Customer.all();
					}]
				}
			})
			.when('/select', {
				templateUrl: 'views/services.html',
				controller: 'ServicesController',
				resolve: {
					location: ['$location', 'StateContainer', 'CustomerLocation', function($location, StateContainer, CustomerLocation) {
						if (StateContainer.has('customer')) {
							var locationId = StateContainer.get('customer').locationId;
							return CustomerLocation.get(locationId);
						} else {
							$location.path('/');
						}
					}]
				} 
			})
			.when('/checkout', {
				templateUrl: 'views/checkout.html',
				controller: 'CheckoutController',
				resolve: {
					customer: ['$location', 'StateContainer', 'Customer', function($location, StateContainer, Customer) {
						if (!StateContainer.has('customer')) {
							$location.path('/');
						}
					}]
				}
			})
			.otherwise({redirectTo: '/'});
	}]);