var angular = require('angular');

module.exports = angular.module('myApp.directives', [])
	.directive('checkList',
		[function() {
			var directive = {
				restrict: 'EA',
				require: '^ngModel',
				scope: {
					ngModel: '='
				},
				templateUrl: 'views/directives/check_list.html'
			}
			
			directive.link = link;
			
			function link(scope, element, attrs) {
				scope.list = [];
				scope.$watch('ngModel', function(list) {
					if (!list) return;
					scope.list = list.filter(function(item) {
						return item[attrs.filterProp] == attrs.filterValue;
					});
				});
			}
			
			return directive;
		}]
	)