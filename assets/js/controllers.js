var angular = require('angular');

module.exports = angular.module('myApp.controllers', [])
	.controller('FrameController',
		['$scope', '$location', 'StateContainer',
			function($scope, $location, StateContainer) {
				$scope.customer = {};
				$scope.links = [];
				
				console.log(StateContainer.has('customer'));
				
				StateContainer.change('customer', function(customer) {
					console.log('change', customer);
					$scope.customer = customer;
				});
				
				$scope.changeCustomer = function() {
					if (StateContainer.has('customer')) {
						StateContainer.set('customer', undefined);
						$scope.customer = {};
					}
					$location.path('/');
				}
			}
		]
	)
	.controller('LoginController',
		['$scope', '$location', 'customers', 'StateContainer', 'Customer',
			function($scope, $location, customers, StateContainer, Customer) {
				$scope.customers = customers;
				
				$scope.selectCustomer = function(customerId) {
					StateContainer.set('customerId', customerId);
					Customer.get(customerId)
						.then(function(customer) {
							StateContainer.set('customer', customer);
							$location.path('/select');
						})
				}
			}
		]
	)
	.controller('ServicesController',
		['$scope', '$location', 'location', 'Catalogue', 'Categories', 'StateContainer',
			function($scope, $location, location, Catalogue, Categories, StateContainer) {
				$scope.location = location;
				$scope.categories = [];
				$scope.catalogue = [];
					
				Categories.all()
					.then(function(categories) {
						$scope.categories = categories;
					})
					
				Catalogue.get(location.id)
					.then(function(catalogue) {
						$scope.catalogue = catalogue;
					});
				
				$scope.checkoutList = [];
				
				$scope.$watch('catalogue', function(l) {
					$scope.checkoutList = l.filter(function(item) {
						return item.selected === true;
					});
				}, true);
				
				$scope.checkout = function() {
					StateContainer.set('checkoutList', $scope.checkoutList);
					$location.path('/checkout');
				};
			}
		]
	)
	.controller('CheckoutController',
		['$scope', 'StateContainer',
			function($scope, StateContainer) {
				$scope.checkoutList = StateContainer.get('checkoutList');
			}
		]
	)