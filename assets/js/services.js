var angular = require('angular');

module.exports = angular.module('myApp.services', [])
	.service('StateContainer',
		[
			function(){
				var dataContainer = {};
				var callbackContainer = {};
				function has(name) {
					return dataContainer.hasOwnProperty(name) && dataContainer[name] !== undefined;
				}
				function set(name, value) {
					dataContainer[name] = value;
					if (callbackContainer.hasOwnProperty(name)) {
						if (angular.isFunction(callbackContainer[name])) {
							callbackContainer[name](value);
						}
					}
				}
				function get(name) {
					if (has(name)) {
						return dataContainer[name];
					}
				}
				function del(name) {
					if (has(name)) {
						delete dataContainer[name];
						
						if (callbackContainer.hasOwnProperty(name)) {
							delete callbackContainer[name];
						}
						
						return true;
					}
					return false;
				}
				function change(name, callback) {
					if (has(name)) {
						callbackContainer[name] = callback;
					} else {
						dataContainer[name] = undefined;
						callbackContainer[name] = callback;
					}
				}
				return {
					has: has,
					set: set,
					get: get,
					del: del,
					change: change
				}			
			}
		]
	)
	.service('Retrive',
		['$q', '$http',
			function($q, $http) {
				function get(url, id) {
					var url = id ? url + id : url;
					var defer = $q.defer();
					$http.get(url)
						.then(function(result) {
							defer.resolve(result.data);
						}, function() {
							defer.reject();
						})
					return defer.promise;
				}
				
				return function(url) {
					return {
						get: function(id) {
							return get(url, id);
						},
						all: function() {
							return get(url);
						}
					}
				}
			}
		]
	)
	.service('Customer',
		['Retrive',
			function(Retrive) {
				var url = '/customer/'; 
				return {
					get: function(id){
						return Retrive(url).get(id);
					},
					all: function(id) {
						return Retrive(url).all();
					}
				};
			}
		]
	)
	.service('CustomerLocation',
		['Retrive',
			function(Retrive) {
				var url = '/location/';
				return {
					get: function(id) {
						return Retrive(url).get(id);
					}
				};
			}
		]
	)
	.service('Catalogue',
		['Retrive',
			function(Retrive) {
				var url = '/catalogue/';
				return {
					get: function(id) {
						return Retrive(url).get(id);
					}
				}
			}
		]
	)
	.service('Categories',
		['Retrive',
			function(Retrive) {
				var url = '/categories/';
				return {
					all: function(id) {
						return Retrive(url).all();
					}
				}
			}
		]
	)