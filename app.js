var fs = require('fs'),
	express = require('express');

var app = express();

app.set('port', 3000);
app.use(express.static('public'));

app.get('/', function(req, res) {
	res.send();
});

fs.readdirSync('./src/controllers').forEach(function (file) {
	if(file.substr(-3) === '.js') {
		var route = require('./src/controllers/' + file)
		route.controller(app);
	}
});

var server = app.listen(app.get('port'), function() {
	var host = server.address().address;
	var port = server.address().port;
	
	console.log('App listening at http://%s:%s', host, port);
});